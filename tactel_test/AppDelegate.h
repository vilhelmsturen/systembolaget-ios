//
//  AppDelegate.h
//  tactel_test
//
//  Created by Vilhelm Sturén on 2016-10-26.
//  Copyright © 2016 Vilhelm Sturén. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

