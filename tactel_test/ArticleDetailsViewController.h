//
//  ViewController.h
//  tactel_test
//
//  Created by Vilhelm Sturén on 2016-10-26.
//  Copyright © 2016 Vilhelm Sturén. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebUtils.h"

@interface ArticleDetailsViewController : UIViewController
@property (strong, nonatomic) NSDictionary *article;

@end

