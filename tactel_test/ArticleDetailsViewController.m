//
//  ViewController.m
//  tactel_test
//
//  Created by Vilhelm Sturén on 2016-10-26.
//  Copyright © 2016 Vilhelm Sturén. All rights reserved.
//

#import "ArticleDetailsViewController.h"

@interface ArticleDetailsViewController ()

@end

@implementation ArticleDetailsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"article %@", self.article);
    
    [self setUpLabels];
    
    
    // Do any additional setup after loading the view, typically from a nib.

    
}

- (void)setUpLabels {
    
    int diff = 25;
    int xpos = 20;
    int labelWidth =290;
    
    //Name
    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xpos, diff, labelWidth, 144)];
    infoLabel.numberOfLines = 1;
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    NSString *infoString = [[self.article objectForKey:@"Namn"]valueForKey:@"text"];
    [infoLabel setText:infoString];
    [self.view  addSubview:infoLabel];
    
    //name2
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xpos, diff*2, labelWidth, 144)];
    infoLabel.numberOfLines = 1;
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    infoString = [[self.article objectForKey:@"Namn2"]valueForKey:@"text"];
    [infoLabel setText:infoString];
    [self.view  addSubview:infoLabel];
    
    //Producent
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xpos, diff*3, labelWidth, 144)];
    infoLabel.numberOfLines = 1;
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    infoString = [[self.article objectForKey:@"Producent"]valueForKey:@"text"];
    [infoLabel setText:infoString];
    [self.view  addSubview:infoLabel];
    
    //Ursprung
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xpos, diff*4, labelWidth, 144)];
    infoLabel.numberOfLines = 1;
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    infoString = [[self.article objectForKey:@"Ursprung"]valueForKey:@"text"];
    [infoLabel setText:infoString];
    [self.view  addSubview:infoLabel];
    
    //Alkoholhalt
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xpos, diff*5, labelWidth, 144)];
    infoLabel.numberOfLines = 1;
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    infoString = [[self.article objectForKey:@"Alkoholhalt"]valueForKey:@"text"];
    [infoLabel setText:infoString];
    [self.view  addSubview:infoLabel];
    
    //Prisinklmoms
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xpos, diff*6, labelWidth, 144)];
    infoLabel.numberOfLines = 1;
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    infoString = [[self.article objectForKey:@"Prisinklmoms"]valueForKey:@"text"];
    [infoLabel setText:infoString];
    [self.view  addSubview:infoLabel];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
