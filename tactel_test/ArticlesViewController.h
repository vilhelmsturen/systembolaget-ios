//
//  ViewController.h
//  tactel_test
//
//  Created by Vilhelm Sturén on 2016-10-26.
//  Copyright © 2016 Vilhelm Sturén. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebUtils.h"
#import "ArticleDetailsViewController.h"

@interface ArticlesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *searchField;

- (NSArray*) getArticlesNames:(NSArray*)articles;

@end

