//
//  ViewController.m
//  tactel_test
//
//  Created by Vilhelm Sturén on 2016-10-26.
//  Copyright © 2016 Vilhelm Sturén. All rights reserved.
//

#import "ArticlesViewController.h"



@implementation ArticlesViewController

NSMutableArray *names;
NSMutableArray *allNames;
NSArray *articles;


- (IBAction)search:(id)sender {
    NSLog(@"sökord %@", self.searchField.text);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self CONTAINS[c] %@",self.searchField.text];
    names = [allNames filteredArrayUsingPredicate:predicate];

    [self.tableView reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.

    articles = [WebUtils getArticles];
    allNames = [self getArticleNames:articles];
    names = allNames;

}

- (NSMutableArray*) getArticleNames: (NSArray*) articles
{
    NSMutableArray *names = [[NSMutableArray alloc] init];
    NSDictionary *nameDict = [articles[3] valueForKey:@"Namn"];
    NSString *tempName;
    
    for(int i = 0; i < articles.count; i++){
        nameDict = [articles[i] objectForKey:@"Namn"];
        tempName = [nameDict valueForKey:@"text"];
        [names addObject:tempName];
    }

    
    return names;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [names count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [names objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ArticleDetailsViewController *artDetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArticleDetailsViewController"];
    
    int index = [self findIndexForName:names[indexPath.row]];
    
    
    artDetViewController.article = articles[index];
    [self.navigationController pushViewController:artDetViewController animated:YES];
}
                       
                           
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int) findIndexForName: (NSString*) name
    {
        int i = 0;
        NSString *tempName = @"";
        while(i<articles.count){
            tempName = [[articles[i] objectForKey:@"Namn"]valueForKey:@"text"];
            if([tempName isEqualToString:name]){
                return i;
            }else{
                i++;
            }
        }
        return -1;
    }
                           


@end
