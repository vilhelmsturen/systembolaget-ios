//
//  WebUtils.m
//  tactel_test
//
//  Created by Vilhelm Sturén on 2016-10-27.
//  Copyright © 2016 Vilhelm Sturén. All rights reserved.
//
#import "WebUtils.h"

@implementation WebUtils

+ (NSArray*) getArticles
{
    NSURL *URL = [NSURL URLWithString:@"https://www.systembolaget.se/api/assortment/products/xml"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:URL];
    NSError *error = nil;
    
    NSDictionary *articlesDictionary = [XMLReader dictionaryForXMLData:data error:&error];
    
    return [[articlesDictionary objectForKey:@"artiklar"]objectForKey:@"artikel"];
    
}

@end
